package rs.ac.bg.etf.distancometer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class DistancometerActivity extends Activity {
    private static final String CONFIG_FILE_NAME = "Distancometer.cfg";
    private static final int CALIBRATION_MENU_OPTION = 0;
    private static final int CHANGELOG_MENU_OPTION = 1;
    private static final int HELP_MENU_OPTION = 2;
    private static final int EXIT_MENU_OPTION = 3;
    private SensorManager sm;
    private CameraPreview camPreview;
    private Button menuButton;
    private Button heightButton;
    FrameLayout framePreview;
    Camera camera;
    Sensor mOrient;
    OrientationListener listener;
    TextView info;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distancometer);
        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mOrient = sm.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        if (mOrient == null) {
            Toast.makeText(this, "Orientation sensor not available!",
                    Toast.LENGTH_LONG).show();
            finish();
        }
        menuButton = (Button) findViewById(R.id.menu);
        heightButton = (Button) findViewById(R.id.measure);
        heightButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                listener.setHeightMode(!listener.getHeightMode());
                heightButton.setText(listener.getHeightMode() ? R.string.ubottom_info
                        : R.string.bottom_info);
            }
        });
        info = (TextView) findViewById(R.id.textView1);
        info.setText("waiting for sensor");
        menuButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                openOptionsMenu();
            }
        });
        listener = new OrientationListener(this);
        readConfig();

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("73B75EA22C5D2708D493D690F95F5D04").build();
        mAdView.loadAd(adRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sm.registerListener(listener, mOrient,
                SensorManager.SENSOR_DELAY_NORMAL);
        camera = getCameraInstance();
        if (camera == null) {
            Toast.makeText(this, "Device does not have camera!",
                    Toast.LENGTH_LONG).show();
            finish();
        }
        camPreview = new CameraPreview(this, camera);
        framePreview = (FrameLayout) findViewById(R.id.camera_preview);
        framePreview.addView(camPreview);
        framePreview.invalidate();
    }

    @Override
    protected void onPause() {
        super.onPause();
        listener.type_of_service = 0;
        sm.unregisterListener(listener);
        if (camera != null)
            camera.release();
        framePreview.removeAllViews();
    }

    @Override
    public void finish() {
        super.finish();
        saveConfig();
        sm.unregisterListener(listener);
    }

    Camera getCameraInstance() {
        Camera c = null;
        c = Camera.open(); // attempt to get a Camera instance
        if (c != null) {
            Camera.Parameters params = c.getParameters();
            c.setParameters(params);
        }
        return c;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, CALIBRATION_MENU_OPTION, CALIBRATION_MENU_OPTION,
                "Calibrate");
        menu.add(0, HELP_MENU_OPTION, HELP_MENU_OPTION, "Help");
        menu.add(0, CHANGELOG_MENU_OPTION, CHANGELOG_MENU_OPTION, "Changelog");
        menu.add(0, EXIT_MENU_OPTION, EXIT_MENU_OPTION, "Exit");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case CALIBRATION_MENU_OPTION:
                showCalibrationDialog();
                return true;
            case HELP_MENU_OPTION:
                showUtilityMenu("Options", R.raw.help);
                return true;
            case CHANGELOG_MENU_OPTION:
                showUtilityMenu("Options", R.raw.changelog);
                return true;
            case EXIT_MENU_OPTION:
                finish();
                return true;
        }
        return false;
    }

    public void saveConfig() {
        PrintWriter writer = null;
        FileOutputStream out = null;
        try {
            File file = new File(getFilesDir(), CONFIG_FILE_NAME);
            out = new FileOutputStream(file);
            writer = new PrintWriter(out);
            writer.write("HEIGHT#" + listener.height);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (Exception e) {
            }
        }
    }

    public void readConfig() {
        BufferedReader reader = null;
        try {
            File file = new File(getFilesDir(), CONFIG_FILE_NAME);
            if (file.exists()) {
                reader = new BufferedReader(new FileReader(getFilesDir() + "/"
                        + CONFIG_FILE_NAME));
                StringTokenizer tokenizer = new StringTokenizer(
                        reader.readLine(), "#");
                tokenizer.nextToken();
                listener.height = Float.parseFloat(tokenizer.nextToken());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (Exception e) {
            }
        }
    }

    private void showUtilityMenu(String title, int raw_file_id) {
        LayoutInflater inflater = getLayoutInflater();
        View util_view = inflater.inflate(R.layout.options_menu, null);
        TextView tv = (TextView) util_view.findViewById(R.id.helpInfo);
        tv.setText(readRawFile(raw_file_id));
        Linkify.addLinks(tv, Linkify.ALL);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setView(util_view);
        builder.setPositiveButton(android.R.string.ok, new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private String readRawFile(int file_id) {
        StringBuilder builder = new StringBuilder();
        InputStream in = getResources().openRawResource(file_id);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String readLine = null;
        try {
            while ((readLine = br.readLine()) != null) {
                builder.append(readLine);
                builder.append("\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                if (br != null)
                    br.close();
                if (in != null)
                    in.close();
            } catch (IOException e) {
            }
        }
        return builder.toString();
    }

    private void showCalibrationDialog() {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.calibration_menu, null);
        final EditText tv = (EditText) view.findViewById(R.id.height_field);
        tv.setText(listener.height + "");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Calibration");
        builder.setView(view);
        builder.setPositiveButton(android.R.string.ok, new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String num = tv.getText().toString();
                if (!tv.equals(""))
                    DistancometerActivity.this.listener.setHeight(Float
                            .parseFloat(num));
            }
        });
        builder.show();
    }

}
