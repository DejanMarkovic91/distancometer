package rs.ac.bg.etf.distancometer;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

/**
 * Manages orientation sensor.
 */
public class OrientationListener implements SensorEventListener {
    float height = 1.4f;
    DistancometerActivity act;
    float axisX, axisY, axisZ;
    float distance;
    float hDistance;
    int type_of_service = 0;
    boolean meassuringHeight = false;

    public OrientationListener(DistancometerActivity act) {
        this.act = act;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // not needed
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        axisX = event.values[0];// AZIMUTH
        axisY = event.values[1];// PITCH
        axisZ = event.values[2];// ROLL
        if (type_of_service == 0) {
            distance = Math.abs((float) (height * Math.tan(axisZ * Math.PI
                    / 180)));
            distance = (float) (Math.round(distance * 10.0) / 10.0);
            if (!meassuringHeight) {
                act.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        act.info.setText("Distance:" + distance);
                    }

                });
                hDistance = distance;
            }
            if (Math.abs(axisY) > 150 && meassuringHeight) {
                type_of_service = 1;// presli horizont
            }
            if (Math.abs(axisY) > 150) act.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    act.info.setText("Please aim at ground!");
                }

            });
            if (meassuringHeight)
                act.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        act.info.setText("Height:"
                                + Math.round((height - hDistance * Math.tan((90 - axisZ) * Math.PI / 180)) * 10.0) / 10.0);
                    }

                });
        } else if (type_of_service == 1) {
            if (Math.abs(axisY) < 10) {
                type_of_service = 0;
            }
            if (meassuringHeight)
                act.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        act.info.setText("Height:"
                                + (height + Math.round(hDistance
                                * Math.tan((90 - axisZ) * Math.PI / 180)
                                * 10.0) / 10.0));
                    }

                });
        } else {
            // to do
        }
    }

    public void setHeightMode(boolean mode) {
        meassuringHeight = mode;
    }

    public boolean getHeightMode() {
        return meassuringHeight;
    }

    public synchronized void setHeight(float height) {
        this.height = height;
    }
}
